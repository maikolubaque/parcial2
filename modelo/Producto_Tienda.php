<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Producto_TiendaDAO.php";
class Producto_Tienda{
    private $tienda;
    private $producto;
    private $stock;
    private $conexion;
    private $Producto_TiendaDAO;
    
    public function getIdProducto_Tienda(){
        return $this -> tienda;
    }
    
    public function getproducto(){
        return $this -> producto;
    }

    
    public function getstock(){
        return $this -> stock;
    }

    
    public function Producto_Tienda($tienda = "", $producto = "",  $stock = ""){
        $this -> tienda = $tienda;
        $this -> producto = $producto;
        $this -> stock = $stock;
        $this -> conexion = new Conexion();
        $this -> Producto_TiendaDAO = new Producto_TiendaDAO($this -> tienda, $this -> producto,  $this -> stock);
    }

    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> Producto_TiendaDAO -> insertar()); 
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> Producto_TiendaDAO -> consultarTodos());
        $Producto_Tiendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto_Tienda($resultado[0], $resultado[1], $resultado[2]);
            array_push($Producto_Tiendas, $p);
        }
        $this -> conexion -> cerrar();        
        return $Producto_Tiendas;
    }
    
    
    
}

?>