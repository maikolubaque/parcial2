<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/TiendaDAO.php";
class Tienda{
    private $idTienda;
    private $nombre;
    private $direccion;
    private $conexion;
    private $TiendaDAO;
    
    public function getIdTienda(){
        return $this -> idTienda;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }

    
    public function getdireccion(){
        return $this -> direccion;
    }

    
    public function Tienda($idTienda = "", $nombre = "",  $direccion = ""){
        $this -> idTienda = $idTienda;
        $this -> nombre = $nombre;
        $this -> direccion = $direccion;
        $this -> conexion = new Conexion();
        $this -> TiendaDAO = new TiendaDAO($this -> idTienda, $this -> nombre,  $this -> direccion);
    }

    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> TiendaDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TiendaDAO -> consultarTodos());
        $Tiendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Tienda($resultado[0], $resultado[1], $resultado[2]);
            array_push($Tiendas, $p);
        }
        $this -> conexion -> cerrar();        
        return $Tiendas;
    }
    
    
    
}

?>