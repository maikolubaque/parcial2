<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;

       
    public function ProductoDAO($idProducto = "", $nombre = "", $cantidad = "", $precio = ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;

    }

    public function consultar(){
        return "select nombre, precio
                from Producto
                where idproducto = '" . $this -> idProducto .  "'";
    }    
    
    public function insertar(){
        return "insert into producto (nombre,  precio)
                values ('" . $this -> nombre . "', '" . $this -> precio . "')";
    }
    
    public function consultarTodos(){
        return "select idproducto, nombre, precio
                from producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idproducto, nombre, precio
                from Producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idproducto)
                from producto";
    }

    public function consultarPorTienda($tienda){
        return "select p.idproducto, p.nombre, pt.stock, p.precio
                from producto p, producto_tienda pt
                where pt.tienda_idtienda = '".$tienda."' and pt.producto_idproducto=p.idproducto";
    }

    
}

?>