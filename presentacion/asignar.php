<?php

$produ = new Producto("", "", "", "");
$productos = $produ->consultarTodos();

$tien = new Tienda("", "", "");
$tiendas = $tien->consultarTodos();

$tienda = "";
if (isset($_POST["tienda"])) {
    $tiendaNombre = $_POST["tienda"];
    $productoNombre = $_POST["producto"];
}
$producto = "";

$stock = "";
if (isset($_POST["stock"])) {
    $stock = $_POST["stock"];
}
if (isset($_POST["crear"])) {
    foreach($productos as $p){
		if($p -> getNombre() == $productoNombre){
			$producto = $p -> getIdProducto();
		}
    }
    
    foreach($tiendas as $t){
		if($t -> getNombre() == $tiendaNombre){
			$tienda = $t -> getIdTienda();
		}
	}


    $Tproducto = new Producto_Tienda($tienda, $producto, $stock);
    $Tproducto->insertar();
}
?>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-3 col-md-0"></div>
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header text-white bg-dark">
                    <h4>Asignar productos a una tienda</h4>
                </div>
                <div class="card-body">
                    <?php if (isset($_POST["crear"])) { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Datos ingresados con exito
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    <?php } ?>
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/asignar.php") ?>" method="post">
                        <label for="Carrera">Producto</label><br>
                        <select id="select-tipo" name="producto">
                            <?php
                            foreach ($productos as $pActual) {
                                echo "<option>";
                                echo "<a class='dropdown-item' href='#'>" . $pActual->getNombre() . "</a>";
                                echo "</option>";
                            }
                            ?>
                        </select></br></br>
                        <label for="Carrera">Tienda</label><br>
                        <select id="select-tipo" name="tienda">
                            <?php
                            foreach ($tiendas as $tActual) {
                                echo "<option>";
                                echo "<a class='dropdown-item' href='#'>" . $tActual->getNombre() . "</a>";
                                echo "</option>";
                            }
                            ?>
                        </select></br>
                        <div class="form-group">
                            <label>Cantidad del producto para esa tienda</label>
                            <input type="number" name="stock" class="form-control" min="1" value="<?php echo $stock ?>" required>
                        </div>
                        <button type="submit" name="crear" class="btn btn-info">Aceptar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>