<?php

?>


<nav class="navbar navbar-expand-md  navbar-dark sticky-top" style="background-color: #0288d1">
  <a class="navbar-brand" href="#">Parcial 2 </a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" aria-expanded="true">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="navb" class="navbar-collapse collapse hide">

    <ul class="navbar-nav">


      <li class="nav-item dropdown active"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Producto</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">Crear</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductos.php") ?>">Consultar</a>
      
        </div>
      </li>

      <li class="nav-item dropdown active"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tienda</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/tienda/crearTienda.php") ?>">Crear</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/tienda/consultarTiendas.php") ?>">Consultar</a>

        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/asignar.php") ?>">Asignar Productos</a>
      </li>

    

      <li class="nav-item">
        <a class="nav-link" href="reporte2.php">generar reporte de todos los productos</a>
      </li>


     
    </ul>
  </div>
</nav>