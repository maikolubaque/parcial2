<?php
$Tienda = new Tienda();
$Tiendas = $Tienda -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Tienda</h4>
				</div>
				<div class="text-right"><?php echo count($Tiendas) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Direccion</th>
							<th>reportes</th>
						</tr>
						<?php 
						$i=1;
						foreach($Tiendas as $TiendaActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $TiendaActual -> getNombre() . "</td>";
						    echo "<td>" . $TiendaActual -> getDireccion() . "</td>";
							echo "<td><a href='reporte.php?". "idTienda=" . $TiendaActual -> getIdTienda(). "&nombre=".$TiendaActual -> getNombre()."' data-toggle='tooltip' data-placement='left' title='inventario para la tienda'><span class='fas fa-edit'></span></a></td>";
						    
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>