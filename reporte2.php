<?php


require_once "modelo/Producto.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Helvetica.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$producto = new Producto();
$productos = $producto -> consultarTodos($idTienda);

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Parcial 2</b>", 20, $opciones);
$pdf -> ezText("<b>Reporte Productos</b>", 16, $opciones);


$encabezados = array("<b>#</b>","<b>Nombre</b>","<b>cantidad</b>","<b>precio</b>");

$datos = array();
$n = 1;
$i = 0;
while($n-- > 0){
    foreach ($productos as $pActual){
        $datos[$i][0] = $i + 1;
        $datos[$i][1] = $pActual -> getNombre();
        $datos[$i][2] = $pActual -> getCantidad();
        $datos[$i][3] = $pActual -> getPrecio();

        $i++;
    }    
}

$opcionesTabla = array(
    "showLines" => 1,
    "shaded" => 1,
    "shadeCol" => array(0.0, 0.8, 1),
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Listado completo de productos", $opcionesTabla);
//$pdf -> ezNewPage();


$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>